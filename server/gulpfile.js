const gulp = require('gulp');
const concat = require("gulp");
const uglify = require("gulp-uglify");

const paths = {
    src: 'src/**/*',
    srcJS: 'src/**/*.js',

    tmp: 'tmp',
    tmpJS: 'tmp/**/*.js',

    dist: "dist",
    distJS: "dist/**/*.js"
}

gulp.task('copy', function () {
    return gulp.src(paths.srcJS).pipe(gulp.dest(paths.tmp));
});

gulp.task('watch', function () {
    gulp.watch(paths.src, ['copy']);
});

gulp.task('default', ['watch']);